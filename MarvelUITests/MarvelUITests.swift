//
//  MarvelUITests.swift
//  MarvelUITests
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import XCTest

class MarvelUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_selectingAnElement() {
        
        let app = XCUIApplication()
        app.otherElements.containing(.image, identifier:"universe").children(matching: .collectionView).element.tap()
        app.navigationBars["Marvel.ComicsPageView"].buttons["Home"].tap()
        
    }
    
}
