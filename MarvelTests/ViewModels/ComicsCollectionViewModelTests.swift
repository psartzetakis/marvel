//
//  ComicsCollectionViewModelTests.swift
//  MarvelTests
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import XCTest
@testable import Marvel

class ComicsCollectionViewModelTests: XCTestCase {
    
    var provider: DomainDecodableDataProvider<ComicJSON>!
    
    override func setUp() {
        super.setUp()
        provider = DomainDecodableDataProvider(settings: MockDomainSettings(), url: "url")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_thatViewModel_isEmpty() {
        // GIVEN: A view model.
        let viewModel = ComicsCollectionViewModel(comicsProvider: provider, results: { _ in })

        // WHEN, THEN: It is freshly initialized it should be empty.
        XCTAssertEqual(viewModel.numberOfItems, 0)
    }
    
    func test_thatViewModel_isNotEmpty() {
        // GIVEN: A view model.
        let viewModel = ComicsCollectionViewModel(comicsProvider: provider, results: { _ in })
       
        // WHEN: There is content.
        let comic = Comic(id: 12, title: "title", description: "desc", images: [], characters: [])
        viewModel.comics.append(comic)
        
        // THEN: It should not be empty.
        XCTAssertEqual(viewModel.numberOfItems, 1)
    }
    
    func test_thatViewModelSubscript_isCorrect() {
        // GIVEN: A view model.
        let viewModel = ComicsCollectionViewModel(comicsProvider: provider, results: { _ in })
        
        // WHEN: With content.
        let comic = Comic(id: 12, title: "title", description: "desc", images: [], characters: [])
        viewModel.comics.append(comic)
        
        // THEN: The subscipt's result should match comic.
        XCTAssertEqual(viewModel[IndexPath(item: 0, section: 0)], comic)
    }

    func test_thatViewModelHasNoImage_forSpecifiedIndexPath() {
        // GIVEN: A view model.
        let viewModel = ComicsCollectionViewModel(comicsProvider: provider, results: { _ in })
        
        // WHEN: With content.
        let comic = Comic(id: 12, title: "title", description: "desc", images: [], characters: [])
        viewModel.comics.append(comic)
        
        // THEN: It should be nil.
        XCTAssertNil(viewModel.imageURL(for: IndexPath(item: 0, section: 0)))
    }

    func test_thatViewModel_shouldFetchMore() {
        // GIVEN: A view model.
        let viewModel = ComicsCollectionViewModel(comicsProvider: provider, results: { _ in })
        
        // WHEN: There is an index big that match the predicate.
       let shouldFetch = viewModel.shouldFetchMore(for: 12)
        
        // THEN: It should be true.
        XCTAssertTrue(shouldFetch)
    }

    func test_thatViewModel_isPreparingForNextBatch_correctly() {
        // GIVEN: A view model.
        let viewModel = ComicsCollectionViewModel(comicsProvider: provider, results: { _ in })
        
        // WHEN: The model is prepared for next batch.
        viewModel.prepareNextBatch()
        
        // THEN: The offset should be increased.
        XCTAssertEqual(viewModel.offset, 20)
    }


}
