//
//  DomainDecodableTests.swift
//  MarvelTests
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import XCTest
@testable import Marvel
import Alamofire
class DomainDecodableTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_thatTheResultOfTheProvider_isCorrect() {
        // GIVEN: A mock DomainDecodableDataProvider
        let expectation = self.expectation(description: #function)
        let mockDomainData = MockDomainDecodableDataProvider<Comic>()
       
        // WHEN: We access the data.
        mockDomainData.getDomainData { result in
            
            // THEN: The result is the expected.
            XCTAssertTrue(result.isSuccess)
            XCTAssertNotNil(result.value)
            XCTAssertEqual(result.value?.id, 12)
            XCTAssertEqual(result.value?.title, "Spiderman")
            XCTAssertEqual(result.value?.description, "Stories...")
            XCTAssertTrue(result.value!.images.isEmpty)
            XCTAssertTrue(result.value!.characters.isEmpty)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2, handler: nil)
    }
    
}
