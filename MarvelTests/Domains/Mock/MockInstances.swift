//
//  MockDomainSettings.swift
//  MarvelTests
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import Foundation
import Alamofire
@testable import Marvel

struct MockDomainSettings: DomainSettingsType {
    public var parameters: [String: Any] {
        return ["a": "v"]
    }
}

struct MockDomainDecodableDataProvider<ComicItem: Decodable>: DomainDecodableDataProviderType {
    
    typealias DataValue = ComicItem
    
    func getDomainData(parameters: [String : Any]? = nil, callBack: @escaping ((Result<ComicItem>) -> Void)) {
        let comic = Comic(id: 12, title: "Spiderman", description: "Stories...", images: [], characters: []) as! ComicItem
        callBack(.success(comic))
    }
}
