//
//  DomainsTests.swift
//  MarvelTests
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import XCTest
@testable import Marvel

class DomainsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_thatDomainSettingsPublicKey_isCorrect() {
        // GIVEN: The settings for a domain.
        let domainSettings = DomainSettings(publicKey: "a", privateKey: "b")
        
        // WHEN: We access the publickey.
        let apikey = domainSettings.publicKey
        
        // THEN: It should match.
        XCTAssertEqual(apikey, "a", "The key should match the parameter")
    }

    func test_thatDomainSettingsConfiguration_isCorrect() {
        // GIVEN: The settings for a domain.
        let domainSettings = DomainSettings(publicKey: "a", privateKey: "b")

        // WHEN: We access the parameters.
        let parameters = domainSettings.parameters
        
        // THEN: It should contain the correct keys.
        XCTAssertEqual(parameters.count, 3, "There should be only 3 keys.")
        XCTAssertNotNil(parameters["ts"], "The key ts should be included.")
        XCTAssertNotNil(parameters["hash"], "The key hash should be included.")
        XCTAssertNotNil(parameters["apikey"], "The key apikey should be included.")
    }
    
    func test_thatMockDomainSettingsConfiguration_isNotCorrect() {
        // GIVEN: The settings for a domain.
        let domainSettings = MockDomainSettings()
        
        // WHEN: We access the parameters.
        let parameters = domainSettings.parameters
        
        // THEN: It should not contain the correct keys.
        XCTAssertEqual(parameters.count, 1, "There should be only 1 keys.")
        XCTAssertNil(parameters["ts"], "The key ts should not be included.")
        XCTAssertNil(parameters["hash"], "The key hash should not be included.")
        XCTAssertNil(parameters["apikey"], "The key apikey should not be included.")
    }
    
    func test_thatDomainsHaveBaseURLs_areSame() {
        // GIVEN: The root domain, and the Comics domain.
        let baseURL = APIDomains.baseURL
        let domainSpecificURL = ComicsDomain.comics.baseURL
        
        // WHEN, THEN: They should have the same baseURL.
        XCTAssertEqual(baseURL, domainSpecificURL)
    }

}
