//
//  ViewController.swift
//  Marvel
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import UIKit
import Kingfisher
import JRTypedStrings

class ComicsCollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityStackView: UIStackView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel: ComicsCollectionViewModel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    let url = ComicsDomain.comics.stringURL

    override func viewDidLoad() {
        super.viewDidLoad()
        toggleActivityIndicator(show: true)
        collectionView.ts.registerNibCell(CoverCollectionViewCell.self)
        let provider = DomainDecodableDataProvider<ComicJSON>(settings: DomainSettings.default, url: ComicsDomain.comics.stringURL)
        viewModel = ComicsCollectionViewModel(comicsProvider: provider, results: { [unowned self] result in
            switch result {
            case .success(let indexPaths):
                if self.collectionView.isHidden { self.toggleActivityIndicator(show: false) }
                self.collectionView.insertItems(at: indexPaths)
            case .failure(let error):
                if self.viewModel.numberOfItems == 0 {
                    self.view.hideAllWith(placeholder: error.localizedDescription)
                }

            }
        })
        viewModel.fetchComics()
    }
    
    func toggleActivityIndicator(show: Bool, animated: Bool = false) {
        imageView.isHidden = show
        collectionView.isHidden = show
        show ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
        activityStackView.isHidden = !show
    }
}


// MARK: - UICollectionViewDataSource
extension ComicsCollectionViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CoverCollectionViewCell = collectionView.ts.dequeueReusableCell(for: indexPath)
        cell.coverImageView.kf.setImage(with: viewModel.imageURL(for: indexPath), placeholder: UIImage(named:"coming_soon"))
        
        return cell
    }
}


// MARK: - UICollectionViewDelegate
extension ComicsCollectionViewController: UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "pageNavigationController") as! UINavigationController
        let pageViewController = navigationController.topViewController as! ComicsPageViewController
        pageViewController.comics = viewModel.comics
        pageViewController.selectedIndex = indexPath.row
        present(navigationController, animated: true, completion: nil)
    }
    
    @objc func dismiss(sender: UIBarButtonItem) {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ComicsCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModel.cellSize(for: collectionView.bounds.size.width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return viewModel.spacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return viewModel.sectionInsets
    }
}


// MARK: - UICollectionViewDataSourcePrefetching
extension ComicsCollectionViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { indexPath in
            if viewModel.shouldFetchMore(for: indexPath.item) {
                viewModel.prepareNextBatch()
                viewModel.fetchComics()
            }
        }
    }
}
