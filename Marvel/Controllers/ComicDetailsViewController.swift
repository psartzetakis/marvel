//
//  ComicDetailsViewController.swift
//  Marvel
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import UIKit

class ComicDetailsViewController: UIViewController {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var comicTitle: UILabel!
    @IBOutlet weak var comicDescription: UITextView!
    var comic: Comic!
    var viewModel: ComicDetailsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ComicDetailsViewModel(comic: comic)
        comicTitle.text = viewModel.title
        comicDescription.text = viewModel.description
        coverImageView.kf.setImage(with: viewModel.imageURL, placeholder: UIImage(named:"coming_soon"))
    }

    

}
