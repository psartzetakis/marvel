//
//  ComicsPageViewController.swift
//  Marvel
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import UIKit
import JRPageViewControllerKit

class ComicsPageViewController: UIViewController {

    var pageViewController: UIPageViewController!
    var pageViewControllerManager: PageViewControllerManager<ComicDetailsViewController>!
    var comics: [Comic] = []
    var selectedIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPageViewController(with: selectedIndex, totalPages: comics.count)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pageViewController.view.frame = view.bounds
    }

    @IBAction func homeButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    private func setupPageViewController(with initialIndex: Int, totalPages: Int) {
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
        addChildViewController(pageViewController)
        view.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
        
        
        let factory: ((Int) -> ComicDetailsViewController?) =  { [unowned self] index in
            let comicDetailsViewController: ComicDetailsViewController = self.storyboard!.ts.instantiate()
            comicDetailsViewController.comic = self.comics[index]
            return comicDetailsViewController
        }
        
        pageViewControllerManager = PageViewControllerManager(insertIn: view, inViewController: self, totalPages: totalPages, initialIndex: initialIndex, viewControllerForIndex: factory)
    }

}
