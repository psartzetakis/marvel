//
//  CoverCollectionViewCell.swift
//  Marvel
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import UIKit

class CoverCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    var borderLayer: CALayer!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        borderLayer = CALayer()
        borderLayer.backgroundColor = UIColor.clear.cgColor
        borderLayer.borderWidth = 5
        borderLayer.borderColor = UIColor.white.cgColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        borderLayer.frame = bounds
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        coverImageView.layer.addSublayer(borderLayer)

    }

}
