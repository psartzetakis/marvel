//
//  Extensions.swift
//  Marvel
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    /// Hashing a string using the MD5.
    ///
    /// - Returns: The hashed string.
    func toMD5() -> String {
        let messageData = self.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes { digestBytes in
            messageData.withUnsafeBytes { messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
}

extension UIView {
    
    /// Hides all the subviews and presents a `UILabel` with a specified message in its center.
    ///
    /// - Parameter placeholder: The message for the `UILabel`.
    func hideAllWith(placeholder: String) {
        subviews.forEach{ $0.isHidden = true }
        let label = UILabel()
        label.text = placeholder
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: self.centerYAnchor)
            ])
    }
}
