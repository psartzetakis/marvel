//
//  Comic.swift
//  Marvel
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import Foundation

/// A struct that represents the `data` key in the root of the JSON.
public struct ComicJSON: Decodable {
    struct ComicData: Decodable {
        let offset: Int
        let limit: Int
        let count: Int
        let total: Int
        let results: [Comic]
    }
    let data: ComicData
}


/// A struct the represents the Image Details that are nested in the comic.
public struct ComicImage {
    let path: String
    let fileExtension: String
}

extension ComicImage: Decodable {
    enum Keys: String, CodingKey {
        case path
        case fileExtension = "extension"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Keys.self)
        path = try values.decode(String.self, forKey: .path)
        fileExtension = try values.decode(String.self, forKey: .fileExtension)
    }

}
// TODO: - A struct that will represent the `Character` of a comic.
struct ComicCharacter {
    let name: String
}


/// A struct that represents the information for a comic.
public struct Comic {
    let id: Int
    var title: String?
    var description: String?
    var images: [ComicImage]
    var characters: [ComicCharacter] = []
    
}

extension Comic: Equatable {
    public static func == (lhs: Comic, rhs: Comic) -> Bool {
        return lhs.id == rhs.id
    }
}

extension Comic: Decodable {
    enum Keys: String, CodingKey {
        case id
        case title
        case description
        case images
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: Keys.self)
        id = try values.decode(Int.self, forKey: .id)
        title = try? values.decode(String.self, forKey: .title)
        description = try? values.decode(String.self, forKey: .description)
        images = try values.decode([ComicImage].self, forKey: .images)
    }
}


