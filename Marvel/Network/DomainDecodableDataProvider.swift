//
//  ComicsProvider.swift
//  Marvel
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import Foundation
import Alamofire

public typealias ResultCallBack<Value> = ((Result<Value>) -> Void)


/// A protocol that defines a type which can return any `Decodable` data type asynchronously.
public protocol DomainDecodableDataProviderType {
    associatedtype DataValue: Decodable
    func getDomainData(parameters: [String: Any]?, callBack: @escaping ResultCallBack<DataValue>)
}


/// A solid `DomainDecodableDataProviderType` instance.
public final class DomainDecodableDataProvider<DecodableData: Decodable>: DomainDecodableDataProviderType {
    
    public typealias Value = DecodableData

    let providerSettings: DomainSettingsType
    let url: String
    
    /// Initializes a `DomainDecodableDataProvider` given the parameters.
    ///
    /// - Parameters:
    ///   - settings: The required settings for the domain.
    ///   - url:      The required url for the domain.
    public init(settings: DomainSettingsType, url: String) {
        providerSettings = settings
        self.url = url
    }
    
    
    /// Get's Data from the server.
    ///
    /// - Parameters:
    ///   - parameters: Any parameters for the request.
    ///   - callBack:   A closure that returns the result of the operation.
    public func getDomainData(parameters: [String: Any]? = nil, callBack: @escaping ResultCallBack<DecodableData>) {
        var params = providerSettings.parameters
        if let parameters = parameters {
            params.merge(parameters) { (current, _) in current }
        }
        Alamofire.request(url, method: .get, parameters: params)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseData { response in
                switch response.result {
                case .success(let jsonData):
                    let comicsJSON = try! JSONDecoder().decode(DecodableData.self, from: jsonData)
                    callBack(.success(comicsJSON))
                case .failure(let error): return callBack(.failure(error))
                }
        }
    }
}
