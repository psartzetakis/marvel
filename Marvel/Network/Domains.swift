//
//  Network.swift
//  Marvel
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import Foundation


/// The protocol that specifies the necessary parameters for getting Authorisation.
public protocol DomainSettingsType {
    
    /// A dictionary that includes the appropriate keys. The keys `ts`, `apikey` and `hash` are required.
    var parameters: [String: Any] { get }
}


/// A solid instance of `DomainSettingsType`.
public struct DomainSettings: DomainSettingsType {
    
    public private(set) var publicKey: String
    private let privateKey: String
    
    static let `default` = DomainSettings(publicKey: "3a972725ce636889be3c63f31a42f7ff", privateKey: "dd6a1e5769489c0ad905614230e93c657568efa5")
    
    public init(publicKey: String, privateKey: String) {
        self.publicKey = publicKey
        self.privateKey = privateKey
    }
    
    public var parameters: [String: Any] {
        let timestamp = String(Date().timeIntervalSince1970)
        let hash =  "\(timestamp)\(privateKey)\(publicKey)".toMD5()
        return ["ts":timestamp, "apikey": publicKey, "hash": hash]
    }
}


/// The list of domains that the app will handle.
///
/// - comics:          A specific subdomain that will include all the `/comic` endoints.
/// - comicCharacters: A specific subdomain that will include all the `/characters` endpoints.
public enum APIDomains {
    case comics(ComicsDomain)
    case comicCharacters(ComicCharactersDomain)
    
    static let baseURL = "http://gateway.marvel.com/v1/public/"
}


/// The enum that will have listed all the `/comic/..` endpoints.
public enum ComicsDomain {
    case comics
    
    var baseURL: String { return APIDomains.baseURL }
   
    var stringURL: String {
        switch self {
        case .comics: return baseURL.appending("/comics")
        }
    }
}

// TODO: Include the endpoints for the `/characters/..` endpoints.
public enum ComicCharactersDomain {}


