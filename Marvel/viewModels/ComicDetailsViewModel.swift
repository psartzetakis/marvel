//
//  ComicDetailsViewModel.swift
//  Marvel
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import Foundation

public struct ComicDetailsViewModel {
    let comic: Comic
    
    init(comic: Comic) {
        self.comic = comic
    }
    
    var title: String {
        return comic.title ?? "No Title"
    }
    
    var description: String {
        return comic.description ?? "No Description"
    }
    
    /// Constructs a URL based for the image of the comic.
    ///
    /// - Parameter indexPath: The indexPath of the comic.
    /// - Returns: The url for the image, if it doesn't exist it returns nil.
    var imageURL: URL? {
        guard let image = comic.images.first else { return nil }
        let stringURL = "\(image.path)/detail.\(image.fileExtension)"
        return URL(string: stringURL)
    }

}
