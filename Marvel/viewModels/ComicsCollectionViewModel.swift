//
//  ComicsCollectionViewModel.swift
//  Marvel
//
//  Created by Panagiotis Sartzetakis on 09/11/2017.
//  Copyright © 2017 Panagiotis Sartzetakis. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


/// A ComicsCollectionViewModel instance.
public final class ComicsCollectionViewModel {
    public typealias CallBack = ((Result<[IndexPath]>) -> Void)

    let comicsProvider: DomainDecodableDataProvider<ComicJSON>
    var comics: [Comic] = []
    
    var updatedResults: CallBack
    
    var offset: Int = 0
    var limit: Int = 20
    
    
    /// Checks if it is time to fetch the next batch of times.
    ///
    /// - Parameter index: The index extracted from the indexPath that is about to be displayed.
    /// - Returns: True if the next batch should be fetched, otherwise false.
    func shouldFetchMore(for index: Int) -> Bool {
        if index - offset > limit / 2 { return true }
        return false
    }
    
    
    /// It updates the parameters for fetching the next batch of comics.
    func prepareNextBatch() {
        offset = offset + limit
    }
    
    /// Initializes a `ComicsCollectionViewModel` given the parameter.
    ///
    /// - Parameters:
    ///   - comicsProvider: The provider that we want to use.
    ///   - results: A closure that is invoked every time there are new data.
    public init(comicsProvider: DomainDecodableDataProvider<ComicJSON>, results: @escaping CallBack) {
        self.comicsProvider = comicsProvider
        self.updatedResults = results
    }
    
    
    /// Fetching comics from the server.
    ///
    /// - Parameter parameters: Any parameters required for the requested.
    func fetchComics(parameters: [String: Any]? = nil) {
        let params: [String: Any] = parameters ?? ["offset": offset, "limit": limit]
        comicsProvider.getDomainData(parameters: params) { [unowned self] result in
            switch result {
            case .success(let comicJSON):
                let newComics = comicJSON.data.results
                var indexPaths: [IndexPath] = []
                for idx in self.comics.count..<self.comics.count + newComics.count {
                    indexPaths.append(IndexPath(item: idx, section: 0))
                }
                self.comics.append(contentsOf: newComics)
                self.updatedResults(.success(indexPaths))
            case .failure(let error):
                self.updatedResults(.failure(error))
            }

        }
    }
    
}

extension ComicsCollectionViewModel {
    
    /// The spacing between cells.
    public var spacing: CGFloat { return 16 }
    
    /// nodoc:
    var numberOfItems: Int { return comics.count }
    
    /// nodoc:
    subscript(indexPath: IndexPath) -> Comic {
        return comics[indexPath.row]
    }
    
    /// Constructs a URL based for the image of the comic.
    ///
    /// - Parameter indexPath: The indexPath of the comic.
    /// - Returns: The url for the image, if it doesn't exist it returns nil.
    func imageURL(for indexPath: IndexPath) -> URL? {
        let comic = self[indexPath]
        guard let image = comic.images.first else { return nil }
        let stringURL = "\(image.path)/portrait_medium.\(image.fileExtension)"
        return URL(string: stringURL)
    }
    
    /// Calculating the size of a cell. It takes as minimum width 100pts and it tries to fit as many as possible respecting any spacing.
    ///
    /// - Parameter width: The available with of the collection view.
    /// - Returns: The size for the cell.
    func cellSize(for width: CGFloat) -> CGSize {
        let ratio: CGFloat = 1.5
        let minimumWidth: CGFloat = 100
        var newWidth = minimumWidth
        let elementsPerRow = Int(width/minimumWidth)
        var availableSpace = width - (CGFloat(elementsPerRow) * minimumWidth)
        availableSpace -= (CGFloat(elementsPerRow + 1) * spacing)
        if availableSpace > 0 {
            newWidth += (availableSpace/(CGFloat(elementsPerRow)))
        }
        return CGSize(width: newWidth, height: newWidth * ratio)
    }
    
    /// nodoc:
    var sectionInsets: UIEdgeInsets {
        return UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
    }

}
