# Marvel

## About

Hi  I am Panos and I present you the required project 🎉

![](screenshot.png?raw=true)
### A bit of heads up
I tried to replicate some of the UI based on the screenshots from GitHub. I have used some dependencies through `cocoapods`, some of these are projects that I have worked on. You can run the tests using `fastlane ios test`.

### Limitations
* The comic `Characters` are not displayed in the  `ComicDetailsViewController`.
* More Tests need to be added (`ComicDetailsViewModel` and more).
* More docs need to be added.
* The `ComicsCollectionViewModel` needs to be abstracted more since it uses `<ComicJSON>` and makes Testing a bit weird..
* When the `ComicDetailsViewController` is being displayed we can possibly display the smaller image (if one exists) until the larger one is ready.
* Ideally a fancy animation when we present the `ComicDetailsViewController`.
* The feature of swiping between different comics from `ComicDetailsViewController` exists however there are the following limitations.

  1. While swiping even if we reach to the end that action will not trigger to fetch more comics. Also the `JRPageViewControllerKit` doesn't support dynamic change of the content at the moment.
  2. It would be good if for example we have swiped to the end, when we return to the `ComicsCollectionViewController` the last comic to be visible.

## Requirements
* Swift 4.0
* Xcode 9+
